package com.thechalakas.jay.framents2;
/*
 * Created by jay on 16/09/17. 5:57 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Fragment1 extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        Log.i("Fragment1","onCreateView reached");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frame1, container, false);
    }
}
