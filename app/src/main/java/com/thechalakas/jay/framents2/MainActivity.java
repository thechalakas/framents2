package com.thechalakas.jay.framents2;

/*
 * Created by jay on 16/09/17. 5:54 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends FragmentActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate reached");
        //create a fragment object
        Fragment1 fragment0 = new Fragment1();
        //get a fragment manager
        android.app.FragmentManager fragmentManager = getFragmentManager();
        //create a transaction
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //add the framer
        fragmentTransaction.replace(R.id.fragment_container,fragment0);
        //commit the changes
        fragmentTransaction.commit();

        //NOW, lets implement a button based system.

        Button buttonFrame1 = (Button) findViewById(R.id.button1);
        Button buttonFrame2 = (Button) findViewById(R.id.button2);

        buttonFrame1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonFrame1 reached");

                //create a fragment object
                Fragment1 fragment1 = new Fragment1();
                //get a fragment manager
                android.app.FragmentManager fragmentManager = getFragmentManager();
                //create a transaction
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //add the framer
                fragmentTransaction.replace(R.id.fragment_container,fragment1);
                //commit the changes
                fragmentTransaction.commit();
            }
        });

        buttonFrame2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonFrame2 reached");

                //create a fragment object
                Fragment2 fragment2 = new Fragment2();
                //get a fragment manager
                android.app.FragmentManager fragmentManager = getFragmentManager();
                //create a transaction
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //add the framer
                fragmentTransaction.replace(R.id.fragment_container,fragment2);
                //commit the changes
                fragmentTransaction.commit();
            }
        });
    }
}
